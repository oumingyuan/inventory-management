module.exports = {
    // baseUrl: './',
    publicPath: './',
    productionSourceMap: false,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://jsonplaceholder.typicode.com',
                changeOrigin: true,
                pathRewrite: {
                    '/api': ''
                }
            },

            //代理1
            '/ms': {
                target: 'https://www.easy-mock.com/mock/592501a391470c0ac1fab128',

                // https://easy-mock.com/mock/5cfc6d5391faee79cbffc1af/example/hello
                // https://www.easy-mock.com/mock/5cfc6d5391faee79cbffc1af/example/hello

                // target: 'https://easy-mock.com/mock/5cfc6d5391faee79cbffc1af',
                changeOrigin: true
            },
            //代理1
            '/example': {
                // target: 'https://www.easy-mock.com/mock/592501a391470c0ac1fab128',

                // https://easy-mock.com/mock/5cfc6d5391faee79cbffc1af/example/hello
                // https://www.easy-mock.com/mock/5cfc6d5391faee79cbffc1af/example/hello

                target: 'https://easy-mock.com/mock/5cfc6d5391faee79cbffc1af',
                changeOrigin: true
            }

        }
    }
};
