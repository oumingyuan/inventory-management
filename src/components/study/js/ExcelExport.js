import axios from 'axios';

// noinspection JSUnusedGlobalSymbols
export default {
    name: "Sort",
    data() {
        return {

            url: 'http://localhost:9891/es/download',

            param: {
                "charge": {
                    "secondPrdId": 10013
                },
                "page": 1,
                "rows": 10000
            }
        }
    },
    methods: {


        download() {
            console.log('download!');

            return new Promise((resolve, reject) => {
                axios.defaults.headers['content-type'] = 'application/json;charset=UTF-8';
                axios({
                    method: 'post',
                    url: 'http://localhost:9891/es/download', // 请求地址
                    data: {
                        "charge": {
                            "secondPrdId": 10013
                        },
                        "page": 1,
                        "rows": 10000
                    }, // 参数
                    responseType: 'blob' // 表明返回服务器返回的数据类型
                }).then(
                    response => {
                        resolve(response.data);
                        let blob = new Blob([response.data], {
                            type: 'application/vnd.ms-excel'
                        });
                        console.log(blob);
                        // noinspection SpellCheckingInspection
                        let fileName = '搜索引擎导出' + (new Date()).getTime() + '.xlsx';
                        if (window.navigator.msSaveOrOpenBlob) {
                            // console.log(2)
                            navigator.msSaveBlob(blob, fileName)
                        } else {
                            // console.log(3)
                            let link = document.createElement('a');
                            link.href = window.URL.createObjectURL(blob);
                            link.download = fileName;
                            link.click();
                            //释放内存
                            window.URL.revokeObjectURL(link.href)
                        }
                    },
                    err => {
                        reject(err)
                    }
                )
            })


        }
    }
}