import axios from 'axios';
// noinspection NpmUsedModulesInstalled
import Global from '@/components/Global'

// noinspection JSUnusedLocalSymbols,JSUnusedGlobalSymbols
export default {
    name: 'bond',
    data() {
        return {
            tableData: [],
            cur_page: 2,
            multipleSelection: [],
            select_cate: '',
            select_word: '',
            del_list: [],
            is_search: false,

            dialogFormVisible: false,
            editVisible: false,
            delVisible: false,
            buyVisible: false,

            idx: -1,
            row: null,
            //新增时候的表单数据
            form: {
                bondId: '',
                amount: '',
                dayRate: '',
                name: '',
                publishTime: this.getFormatDate(),
                publisher: sessionStorage.getItem('userName'),
                dueDate: '',
                balance: '',
                purchaseAmount: null
            },


            rules: {
                amount: [
                    {required: true, message: '请输入正确的金额', trigger: 'blur'},
                    {min: 1, max: 6, message: '金额不能大于十万', trigger: 'blur'},
                    {pattern: /^[1-9]\d*$/, message: '金额必须是正整数', trigger: 'blur'}
                ],
                dayRate: [
                    {required: true, message: '请输入供货商负责人', trigger: 'blur'},
                    {min: 1, max: 4, message: '长度最多只能是四个字符', trigger: 'blur'},
                    {
                        pattern: /^(([^0][0-9]+|0)\.([0-9]{1,2})$)|^(([^0][0-9]+|0)$)|^(([1-9]+)\.([0-9]{1,2})$)|^(([1-9]+)$)/,
                        message: '金额是数字',
                        trigger: 'blur'
                    }
                ],
                name: [
                    {required: true, message: '请输入正确的债券名称', trigger: 'blur'},
                    {min: 1, max: 10, message: '金额不能大于十个字符', trigger: 'blur'}
                ],
                publishTime: [
                    {required: true, message: '请输入正确的债券名称', trigger: 'blur'},
                ],
                dueDate: [
                    {required: true, message: '请输入债券到期日期', trigger: 'blur'},

                ],
            },
            rulesBuy: {
                purchaseAmount: [
                    {required: true, message: '请输入正确的金额', trigger: 'blur'},
                    // {pattern: /^[1-9]\d*$/, message: '金额必须是正整数', trigger: 'blur'},
                    // {min: 1, max: 6, message: '金额不能大于十万', trigger: 'blur'},
                    {validator: check, trigger: 'blur'},
                ],
            },
            formLabelWidth: '120px'
        };

        //校验函数
        function check(rule, value, callback) {
            setTimeout(() => {
                if (!Number(value)) {
                    callback(new Error('请输入正整数'));
                } else {
                    const re = /^[0-9]*[1-9][0-9]*$/;
                    const rsCheck = re.test(value);
                    if (!rsCheck) {
                        callback(new Error('请输入正整数'));
                    } else {
                        callback();
                    }
                }
            }, 1000);
        }
    },

    created() {
        this.getData();
    },
    computed: {
        data() {
            return this.tableData
        }
    },
    methods: {


        //方法区
        formatDate(row, column) {
            // 获取单元格数据
            let data = row[column.property];
            if (data == null) {
                return null
            }
            let date = new Date(data);
            let Y = date.getFullYear() + '-';
            let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
            let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
            return Y + M + D;
        },
        handleCurrentChange(val) {
            // noinspection JSUnusedGlobalSymbols
            this.cur_page = val;
            this.getData();
        },

        getData() {
            axios({
                method: 'get',
                url: '/proxy/finance/bond/bonds/0',
                data: {},
                headers: {
                    'xxl_sso_sessionid': sessionStorage.getItem('sessionId')
                }
            }).then(res => {
                if (res.data.code === 500) {
                } else {
                }
                console.log(res.data.content);
                this.tableData = res.data.content;
            }).catch(error => {
                console.log(error);
            });

        },
        search() {
            // noinspection JSUnusedGlobalSymbols
            this.is_search = true;
        },
        formatter(row, column) {
            return row.address;
        },
        filterTag(value, row) {
            return row.tag === value;
        },
        handleEdit(index, row) {
            this.idx = index;
            const item = this.tableData[index];
            this.form = {
                name: item.name,
                date: item.date,
                address: item.address
            };
            // noinspection JSUnusedGlobalSymbols
            this.editVisible = true;
        },

        delAll() {
            const length = this.multipleSelection.length;
            let str = '';
            this.del_list = this.del_list.concat(this.multipleSelection);
            for (let i = 0; i < length; i++) {
                str += this.multipleSelection[i].name + ' ';
            }
            this.$message.error('删除了' + str);
            this.multipleSelection = [];
        },
        handleSelectionChange(val) {
            this.multipleSelection = val;
        },
        // 保存编辑
        saveEdit() {
            this.$set(this.tableData, this.idx, this.form);
            // noinspection JSUnusedGlobalSymbols
            this.editVisible = false;
            this.$message.success(`修改第 ${this.idx + 1} 行成功`);
        },

        getFormatDate() {
            const date = new Date();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            return date.getFullYear() + "-" + month + "-" + strDate;
        },


        //add

        handleAdd() {

            this.form = {
                amount: '',
                dayRate: '',
                name: '',
                publishTime: this.getFormatDate(),
                publisher: sessionStorage.getItem('userName'),
                dueDate: ''
            };
            // noinspection JSUnusedGlobalSymbols
            this.dialogFormVisible = true;
        },


        submitForm(formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    /**
                     * 向客户端发送请求来新增供应商信息
                     */
                    this.$axios({
                        method: 'post',
                        url: '/proxy/finance/bond/bond',
                        data: this.form,
                        headers: {
                            'xxl_sso_sessionid': sessionStorage.getItem('sessionId')
                        }
                    }).then(response => {
                        if (response.data.code === 200) {
                            this.$message.success('提交成功！');
                            // noinspection JSUnusedGlobalSymbols
                            this.dialogFormVisible = false;  //关闭弹窗

                            //  新增后需要删除弹窗中的数据
                            this.form.amount = '';
                            this.form.dayRate = '';
                            this.form.name = '';
                            this.form.publishTime = this.getFormatDate();
                            this.form.publisher = sessionStorage.getItem('userName');
                            this.form.dueDate = '';


                            this.getData()
                        } else {
                            this.$message.error("后端新增失败");
                        }
                    }).catch(error => {
                        console.log(error);
                        this.$message.error(error);
                    });
                } else {
                    this.$message.warning('请填写正确的信息！');
                    return false;
                }
            });
        },


        //删除-弹出框的数据获取
        handleDelete(index, row) {
            this.idx = index;
            this.row = row;
            this.delVisible = true;
        },
        //删除-弹出框的操作
        deleteRow() {
            // this.tableData.splice(this.idx, 1);

            // 判断是否有删除权限
            let publisher = this.row.publisher;
            let code = sessionStorage.getItem('userName');
            if (publisher !== code) {
                this.$message.warning("你没有权限删除别人的债券");
                return
            }
            // noinspection JSUnresolvedVariable
            this.$axios({
                method: 'delete',
                url: '/proxy/finance/bond/bonds/' + this.row.bondId,
                headers: {
                    'xxl_sso_sessionid': sessionStorage.getItem('sessionId')
                }
            }).then(response => {
                console.log(response.data);
                if (response.data.code === 200) {
                    this.$message.success(response.data.data);
                    //总数减1
                    // this.currentTotal--;

                    this.getData()
                } else {
                    this.$message.success('删除失败');
                }
            }).catch(error => {
                console.log(error);
                this.$message.error(error);
            });
            //关闭删除框
            this.delVisible = false;
            console.log("是否显示删除框：", this.delVisible);
        },
        handleBuy(index, row) {
            this.idx = index;
            const item = this.tableData[index];
            this.form = {
                bondId: item.bondId,
                amount: item.amount,
                balance: item.balance,
                dayRate: item.dayRate,
                name: item.name,
                publishTime: item.publishTime,
                publisher: item.publisher,
                dueDate: item.dueDate,
                purchaseAmount: item.balance.toString()
            };

            // noinspection JSUnusedGlobalSymbols
            this.buyVisible = true;
        },

        buyBond(formName) {

            this.$refs[formName].validate((valid) => {
                if (valid) {
                    //首先是数据校验   购买的金额不可以大于债券剩余金额
                    if (this.form.purchaseAmount > this.form.balance) {
                        this.$message.error("购买的金额不可以大于债券剩余金额");
                        return
                    }

                    //然后是自己无法购买自己的债券
                    //当前登录的人
                    if (sessionStorage.getItem('userName') === this.form.publisher) {
                        this.$message.error("自己无法购买自己的债券");
                        return
                    }

                    /**
                     * 向客户端发送请求来新增供应商信息
                     */
                    this.$axios({
                        method: 'post',
                        url: '/proxy/finance/purchase/buy',
                        params: {
                            amount: this.form.purchaseAmount,
                            bondId: this.form.bondId,
                            purchaser: sessionStorage.getItem('userName'),
                        },
                        headers: {
                            'xxl_sso_sessionid': sessionStorage.getItem('sessionId')
                        }
                    }).then(response => {
                        if (response.data.code === 200) {
                            this.$message.success('债券购买成功！');
                            // noinspection JSUnusedGlobalSymbols
                            this.buyVisible = false;  //关闭弹窗
                            this.getData()
                        } else {
                            this.$message.error("债券购买失败");
                        }
                    }).catch(error => {
                        console.log(error);
                        this.$message.error(error);
                    });
                }

            })


        }


    }
}