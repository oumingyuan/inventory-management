// noinspection JSUnusedLocalSymbols,JSUnusedGlobalSymbols
export default {
    name: 'Select',
    data() {
        return {
            url: '',  //查询数据的url地址
            tableData: [],//数据存放对象

            currentPage: 1,//当前的数据是第几页
            currentTotal: null,
            pageSize: 15,


            multipleSelection: [],//多选

            select_cate: '',//删选省份
            select_word: '',//删选地址和名称

            del_list: [],//删除列表

            is_search: false,
            editVisible: false,
            delVisible: false, //删除弹出框的显示

            form: {
                company_name: '',
                name: '',
                date: '',
                address: ''
            },

            idx: -1,
            row: null,

            rules: {
                supplierLeader: [
                    {required: true, message: '请输入供货商负责人', trigger: 'blur'},
                    {min: 1, max: 50, message: '长度在 1 到 50个字符', trigger: 'blur'}
                ],

                leaderSex: [
                    {required: true, message: '请输入供货商负责人的性别', trigger: ['blur', 'change']},
                    {type: 'text', message: '请输入正确的邮箱地址', trigger: ['blur', 'change']},
                ],

                supplierCompanyName: [
                    {required: true, message: '请输入供货商公司', trigger: 'blur'},
                    {min: 1, max: 50, message: '长度在 1 到 50个字符', trigger: 'blur'}
                ],

                leaderEmail: [
                    {required: true, message: '请输入邮箱地址', trigger: 'blur'},
                    {type: 'email', message: '请输入正确的邮箱地址', trigger: ['blur', 'change']},
                    {min: 1, max: 50, message: '长度在 1 到 50个字符', trigger: 'blur'}
                ],

                contactInformation: [
                    {required: true, message: '请输入联系方式', trigger: 'blur'},
                    {pattern: /^1[34578]\d{9}$/, message: '请输入11位正确的手机号', trigger: 'blur'}
                ],

                url: [
                    {required: false, message: '请输入网址', trigger: 'blur'},
                    {type: 'url', message: '请输入正确的url地址', trigger: 'blur'},
                    {min: 1, max: 50, message: '长度在 1 到 50个字符', trigger: 'blur'}
                ],

                address: [
                    {required: true, message: '请输入通讯地址', trigger: 'blur'},
                    {min: 1, max: 50, message: '长度在 1 到 50个字符', trigger: 'blur'}
                ],

                remark: [
                    {min: 0, max: 1000, message: '长度在 0 到 1000个字符', trigger: 'blur'}
                ],


            }


        }
    },
    created() {
        this.getData();
    },
    computed: {
        data() {
            return this.tableData.filter((d) => {

                //删除标志
                let is_del = false;
                for (let i = 0; i < this.del_list.length; i++) {
                    if (d.name === this.del_list[i].name) {
                        is_del = true;
                        break;
                    }
                }


                // 筛选
                if (!is_del) {
                    if (

                        // 在地址里模糊筛选省份
                        d.address.indexOf(this.select_cate) > -1 &&

                        // 筛选名称和地址
                        // name
                        (
                            d.supplierCompanyName.indexOf(this.select_word) > -1 ||
                            d.address.indexOf(this.select_word) > -1
                        )
                    ) {
                        return d;
                    }
                }
            })
        }
    },
    methods: {

        // formatRole: function(row, column) {
        //     return row.leaderSex == 0 ? "男" : row.leaderSex == 1 ? "女" : "aaa";
        // },

        handleSizeChange(val) {
            // noinspection JSUnusedGlobalSymbols
            this.pageSize = val;
            console.log(`每页 ${val} 条`);
            this.getData();
        },
        // 分页导航
        handleCurrentChange(val) {

            this.currentPage = val;
            console.log(`当前页: ${val}`);


            // this.cur_page = val;
            this.getData();
        },


        // 获取 easy-mock 的模拟数据
        getData() {
            // 开发环境使用 easy-mock 数据，正式环境使用 json 文件
            if (process.env.NODE_ENV === 'development') {
                // this.url = '/ms/table/list';
                this.url = '/example/hello';
            }


            /**
             * 获取easy-mock 数据
             */
            // this.$axios.post(this.url, {
            //     page: this.cur_page
            // }).then((res) => {
            //
            //     console.log(res.data.list);
            //     this.tableData = res.data.list;
            // })


            this.url = 'http://localhost:8081/inventory/suppliers/findSupplier';
            console.log("this.url", this.url);

            this.$axios({
                method: 'post',
                url: this.url,
                data: {
                    userID: "1",
                    supplierCompanyName: "",
                    supplierLeader: "",
                    pageNum: this.currentPage.toString()
                }
            }).then(response => {

                console.log(response);

                // console.log("houduan shuju ", response.data.data.list);

                this.tableData = response.data.data.list;
                // noinspection JSUnusedGlobalSymbols
                this.currentTotal = response.data.data.total;
            }).catch(error => {
                console.log(error);
                this.$message.error(error);
            });


        },
        search() {
            this.is_search = true;
            console.log(this.is_search)
        },
        formatter(row, column) {
            return row.address;
        },
        filterTag(value, row) {
            return row.tag === value;
        },
        handleEdit(index, row) {
            this.idx = index;
            const item = this.tableData[index];

            //防止没有写备注产生空指针异常
            // let remarks = "";
            // if (item.remarks[0]) {
            //     remarks = item.remarks[0].body
            // } else {
            //     remarks = ""
            // }

            // money=money>=500?total:money;

            this.form = {
                // remarks: remarks,
                remarks: item.remarks[0] ? item.remarks[0].body : "",//防止没有写备注产生空指针异常
                url: item.url,
                contactInformation: item.contactInformation,
                supplierLeader: item.supplierLeader,
                supplierCompanyName: item.supplierCompanyName,  //供应商公司
                leaderSex: item.leaderSex,
                leaderEmail: item.leaderEmail,
                name: item.name,
                date: item.date,
                address: item.address,

                id: item.id
            };


            this.editVisible = true;
            console.log("编辑弹出框：", this.editVisible)
        },

        handleDelete(index, row) {
            this.idx = index;
            this.row = row;
            this.delVisible = true;
        },

        /**
         * 批量删除删除操作
         */
        delAll() {
            // const length = this.multipleSelection.length;
            // let str = '';
            // this.del_list = this.del_list.concat(this.multipleSelection);
            // for (let i = 0; i < length; i++) {
            //     str += this.multipleSelection[i].name + ' ';
            // }
            // this.$message.error('删除了' + str);
            // this.multipleSelection = [];

            this.$message.success('暂时不确定是否开发');


        },
        handleSelectionChange(val) {
            // noinspection JSUnusedGlobalSymbols
            this.multipleSelection = val;
        },
        // 保存编辑
        saveEdit() {


            this.$set(this.tableData, this.idx, this.form);
            this.editVisible = false;

            console.log("form ", this.form);

            console.log("form ", this.form.id);


            // debugger;
            let form_data = {

                userID: "1",
                supplierCompanyName: "123",
                supplierLeader: "",
                leaderSex: "",
                leaderEmail: "",
                contactInformation: "",
                address: "",
                supplierID: "25",
                operatingPerson: "欧明跃"



                // supplierCompanyName: this.form.supplierCompanyName,
                // supplierLeader: this.form.supplierLeader,
                // leaderSex: this.form.leaderSex,
                // leaderEmail: this.form.leaderEmail,
                // contactInformation: this.form.contactInformation,
                // address: this.form.address,
                // supplierID: this.form.id.toString(),
                // operatingPerson: "欧明跃"
            };

            console.log("form_data:" + form_data);

            //发送http请求把远程服务器数据改掉

            this.$axios({
                method: 'post',
                url: '//47.100.211.254:8081/inventory/suppliers/modifySupplier',
                data: form_data

            }).then(response => {


                console.log("response.data: " + response.data);

                // this.tableData = response.data.data.list;
                // noinspection JSUnusedGlobalSymbols
                // this.currentTotal = response.data.data.total;
            }).catch(error => {//
                console.log(error);
                this.$message.error(error);
            });

            // this.$set(this.tableData, this.idx, this.form);

            this.getData();
            this.$message.success(`修改第 ${this.idx + 1} 行成功`);
        },


        /**
         *确定删除
         */
        deleteRow() {
            this.tableData.splice(this.idx, 1);
            console.log(this.row.id);


            //发送请求删除数据库

            this.$axios({
                method: 'delete',
                url: "//47.100.211.254:8081/inventory/suppliers/deleteSupplier",
                data: {
                    userID: "1",
                    supplierID: [this.row.id.toString()]
                }
            }).then(response => {
                // console.log(response.data.data.list);
                //
                // this.tableData = response.data.data.list;
                // // noinspection JSUnusedGlobalSymbols
                // this.currentTotal = response.data.data.total;

                console.log(response.data);

                if (response.data.code === "200") {
                    this.$message.success('删除成功');

                    //总数减1
                    this.currentTotal--;

                } else {
                    this.$message.success('删除失败');
                }


            }).catch(error => {
                console.log(error);
                this.$message.error(error);
            });


            //关闭删除框
            this.delVisible = false;
            console.log("是否显示删除框：", this.delVisible);


        },


        /**
         * ElementUI的表单验证(二)之dialog关闭验证
         * @param form
         */
        closeDialog: function (form) {
            // noinspection JSUnusedGlobalSymbols
            this.dialogFormVisible = false;

            this.$refs[form].resetFields();//将form表单重置
        }
        ,
    }
}

